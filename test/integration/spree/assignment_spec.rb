require "selenium-webdriver"
require "test/unit"
require "byebug"


class LoginClass < Test::Unit::TestCase

  def setup
    Selenium::WebDriver::Chrome.driver_path = '/usr/local/bin/chromedriver'
    @driver = Selenium::WebDriver.for :chrome
    @driver.get('http://spree-tdd-jfanals.c9users.io')
    #@driver.manage.window.maximize

    @wait = Selenium::WebDriver::Wait.new(:timeout => 10)
  end

  def teardown
    @driver.quit
  end

  def jump_c9_intro
    @wait.until {
      /Open the App/.match(@driver.page_source)
    }

    @driver.find_elements(:xpath, "//*[contains(text(), 'Open the App')]")[0].click()
  end

  def go_home
    @driver.get('http://spree-tdd-jfanals.c9users.io')

  end

  def add_to_cart(product)
    go_home

    @wait.until {
      /Cart/.match(@driver.page_source)
    }
    @driver.find_elements(:xpath, "//*[contains(text(), '#{product}')]")[0].click()

    @wait.until {
      /Add To Cart/.match(@driver.page_source)
    }
    @driver.find_element(:id, "add-to-cart-button").click()

  end

  def test_assignment_test_data_1
    jump_c9_intro

    add_to_cart("Lavender")
    add_to_cart("cufflinks")
    add_to_cart("T-shirt")

    assert(@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (10 Percent Discount)"), "There should be 10% discount promotion")
    assert(!@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (Lavender heart promo)"), "There should be NO Lavender promotion")
    assert(@driver.find_element(:class => "cart-total").text.include?("$66.78"), "Wrong total")
  end

  def test_assignment_test_data_2
    jump_c9_intro

    add_to_cart("Lavender")
    add_to_cart("T-shirt")
    add_to_cart("Lavender")

    assert(!@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (10 Percent Discount)"), "There should be NO 10% discount promotion")
    assert(@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (Lavender heart promo)"), "There should be Lavender promotion")
    assert(@driver.find_element(:class => "cart-total").text.include?("$36.95"), "Wrong total")
  end

  def test_assignment_test_data_3
    jump_c9_intro

    add_to_cart("Lavender")
    add_to_cart("cufflinks")
    add_to_cart("Lavender")
    add_to_cart("T-shirt")

    assert(@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (10 Percent Discount)"), "There is should be 10% discount promotion")
    assert(@driver.find_element(:id => "cart_adjustments").text.include?("Promotion (Lavender heart promo)"), "There should be Lavender promotion")
    assert(@driver.find_element(:class => "cart-total").text.include?("$73.60"), "Wrong total")


  end
end
