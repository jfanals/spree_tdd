module Spree
  class Promotion
    module Rules
      class TwoOrMorePromotionRule < Spree::PromotionRule
        def applicable?(promotable)
          promotable.is_a?(Spree::Order)
        end
      
        def eligible?(order, options = {})
            true
        end
      
        def actionable?(line_item)
          # debugger if line_item.quantity >= 2
          line_item.quantity >= 2
        end
      end
    end
  end
end