require 'rails_helper'

RSpec.describe 'The Food Market: ', type: :feature do

  let!(:product_lavender) { create(:product, name: "Lavender", price: 9.25) }
  let!(:product_cufflinks) { create(:product, name: "Cufflinks", price: 45) }
  let!(:product_tshirt) { create(:product, name: "T-shirt", price: 19.95) }

  def add_lavender_to_cart
    visit spree.root_path
    click_link "Lavender"
    click_button "add-to-cart-button"
  end

  def add_cufflinks_to_cart
    visit spree.root_path
    click_link "Cufflinks"
    click_button "add-to-cart-button"
  end

  def add_tshirt_to_cart
    visit spree.root_path
    click_link "T-shirt"
    click_button "add-to-cart-button"
  end

  before do
    # Promotion only for Lavender only if two or more
    promotion_lav = Spree::Promotion.create!(name:       "Lavender 0.75 per item if two or more",
                                            starts_at:  1.day.ago,
                                            expires_at: 1.day.from_now)

    rule_lav = Spree::Promotion::Rules::Product.create(products: [product_lavender])
    calculator_lav = Spree::Calculator::FlexiRate.new(preferred_first_item: 0.75, preferred_additional_item: 0.75)
    action_lav = Spree::Promotion::Actions::CreateItemAdjustments.create(calculator: calculator_lav)
    promotion_lav.rules << rule_lav
    # Two or more rule in: app/models/spress/promotion/rules/two_or_more_promotion_rule.rb
    promotion_lav.rules << Spree::Promotion::Rules::TwoOrMorePromotionRule.create
    promotion_lav.actions << action_lav

    # Standard 10% of if more than 60 pounds
    promotion_10 = Spree::Promotion.create!(name:       "10% off if +60 pounds",
                                         starts_at:  1.day.ago,
                                         expires_at: 1.day.from_now)

    rule_10 = Spree::Promotion::Rules::ItemTotal.create(preferred_operator_min: 'gt', preferred_amount_min: 50, preferred_operator_max: 'lt', preferred_amount_max: 10000)
    calculator_10 = Spree::Calculator::FlatPercentItemTotal.new(preferred_flat_percent: 10)

    action_10 = Spree::Promotion::Actions::CreateItemAdjustments.create(calculator: calculator_10)
    promotion_10.rules << rule_10
    promotion_10.actions << action_10

  end

  # it "Check that 10% promotion is applied" do
  #   add_cufflinks_to_cart
  #   add_tshirt_to_cart
  #
  #   visit '/cart'
  #   expect(page).to have_content("10% off if +60 pounds")
  #   expect(page).not_to have_content("Lavender 0.75 per item if two or more")
  #   expect(page).to have_content("Subtotal (2 items) $64.95")
  #   expect(page).to have_content("Total $58.45")
  # end
  #
  # it "Check that no promotion is applied" do
  #   add_lavender_to_cart
  #
  #   visit '/cart'
  #   expect(page).not_to have_content("10% off if +60 pounds")
  #   expect(page).not_to have_content("Lavender 0.75 per item if two or more")
  #   expect(page).to have_content("Total $9.25")
  # end
  #
  # it "Check that Lavender promotion is applied" do
  #   add_lavender_to_cart
  #   add_lavender_to_cart
  #
  #   visit '/cart'
  #   expect(page).not_to have_content("10% off if +60 pounds")
  #   expect(page).to have_content("Lavender 0.75 per item if two or more")
  #   expect(page).to have_content("Subtotal (2 items) $18.50")
  #   expect(page).to have_content("Total $17.00")
  # end
  #
  # it "Assignment Test Data #1" do
  #   add_lavender_to_cart
  #   add_cufflinks_to_cart
  #   add_tshirt_to_cart
  #
  #   visit '/cart'
  #   expect(page).to have_content("10% off if +60 pounds")
  #   expect(page).not_to have_content("Lavender 0.75 per item if two or more")
  #   expect(page).to have_content("Subtotal (3 items) $74.20")
  #   # expect(page).to have_content("Total $66.78")
  #   expect(page).to have_content("Total $66.77")
  # end
  #
  # it "Assignment Test Data #2" do
  #   add_lavender_to_cart
  #   add_tshirt_to_cart
  #   add_lavender_to_cart
  #
  #   visit '/cart'
  #   expect(page).not_to have_content("10% off if +60 pounds")
  #   expect(page).to have_content("Lavender 0.75 per item if two or more")
  #   expect(page).to have_content("Subtotal (3 items) $38.45")
  #   expect(page).to have_content("Total $36.95")
  # end

  it "Assignment Test Data #3" do
    add_lavender_to_cart
    add_cufflinks_to_cart
    add_lavender_to_cart
    add_tshirt_to_cart

    visit '/cart'
    debugger
    expect(page).to have_content("10% off if +60 pounds")
    expect(page).to have_content("Lavender 0.75 per item if two or more")
    # expect(page).to have_content("Subtotal (4 items) $83.45")
    # expect(page).to have_content("Total $73.76")
    # by default Spree applies first the 0.75 discount, then the 10% global
    # expect(page).to have_content("Total $75.10")
  end

end